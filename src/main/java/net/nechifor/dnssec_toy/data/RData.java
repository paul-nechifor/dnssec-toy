package net.nechifor.dnssec_toy.data;

public abstract class RData
{
    public abstract byte[] toBytes();
}
